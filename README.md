CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Slot Configuration
* Slot Content Configuration
* Create Slot Content
* Maintainers


INTRODUCTION
------------

The slots module aims to provide a "slot" functionality which can be used and
placed anywhere and allows "pushing" content based on conditions into these
placeholders.

This can be useful in different scenarios. Here some example user stories:
* You want to have a placeholder in a layout, where your content creator can
  push content into, for example a CTA based on a node type.
* In a default layout you have a piece of content, which you don't want to be
  exported into your configuration yml files. Place a slot instead and add the
  content later.
* You want to show CTA's in a paragraph content structure, but it needs to be
  flexible. Use slots_paragraph to place a slot and let it load condition based
  content instead.
* You need a piece of content above your view and don't want it to be getting
  exported into your configuration. Use a slot instead and add the content
  later.


REQUIREMENTS
------------

This module requires the following modules:

* Serialization (core)
* Plugin config form builder
  * https://www.drupal.org/project/plugin_config_form_builder
* Block plugin view builder
  * https://www.drupal.org/project/block_plugin_view_builder
* Conditions (Conditions field submodule)
  * https://www.drupal.org/project/conditions


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


SLOT CONFIGURATION
-------------

In order to use this module, you can choose between different options on how to
place a slot.
* Place a slot block via Block UI or Layout Builder.
* Use it with Paragraphs (requires the slots_paragraphs submodule).
* Use it with Views (requires the slots_views submodule).
* Directly in your .twig file (requires the twig_views submodule).


A slot currently consist out of two configurable options:
1. The slot identifier (slot_id):
   The ID is used to identify a slot. It can be unique, but it is not required.
   It is perfectly fine to use the same ID in different context.
2. The cardinality:
   Defines how many blocks can be rendered through a slot.

**Usage with Block UI**
1. Go to the region you'd like to add a slot add click "add block".
2. Search for the "Slot block" block and place the block.
3. Configure the slot identifier and slot cardinality.
4. Click "Save block".

**Usage with Layout Builder**
1. Go to the region you'd like to add a slot add click "Place block".
2. The settings-tray will open on the right side to let you choose a block, a
   new create button should be available at the top "+ Create slot" - click it!
3. Configure the slot identifier and slot cardinality.
4. Click "Add block".

**Usage with Views**
1. Go to the view you'd like to add a slot to. Slots can be added to the header
   or the footer.
2. Click on "Add" at the desired place and search for "Slot" and click "Add and
   configure".
3. Configure the slot identifier and slot cardinality.
4. Click "Apply".

**Usage with TWIG**
1. Edit the twig template you'd like to add a slot to.
2. Use the slot() twig function as following:
   ```
    {{ slot(slot_id, cardinality) }}
   ```

You now have a slot in place, lets proceed and create some content for it!


SLOT CONTENT CONFIGURATION
-------------

Currently only content blocks can be used as slot content. In order to use a
block type as slot content, you only need to add the "slot" field to it.
1. Make sure the slot has been rendered in the web browser at least once.
2. Go to /admin/structure/block-content.
3. Select the block type you'd like to use and click on "Manage fields".
4. On the manage fields page click "Create new field" to add a new field.
5. Click on "Slots" to add a slot field and "Continue".
6. Give the field a label, it's not important how it's name is, "Slots" is a
   good name to start with.
7. Click "Save settings" on the next page and you are ready to create some
   slot content!


CREATE SLOT CONTENT
-------------

1. Go to /admin/content/block and click "Add content block".
2. Select the block type you added the field to, if you only have one block type
   you will get redirected directly to the form.
3. Fill out your fields with the content and then proceed to the "Slots"
   fieldset where you check the "This content shall be displayed in slots"
   checkbox.
4. Configure the conditions for your use-case. The condition configuration UI
   can be a bit overwhelming, but it gives you great power and flexibility to
   decide precisely when the block should be displayed in your slot. Make sure
   to add a "Slot" condition as well with the identifier you have chosen for
   your slot in the "SLOT CONFIGURATION" step earlier. This is where the magic
   happens.
5. Save your block!

The block should now appear in the slot you created.


MAINTAINERS
-----------

Current maintainers:
* Pascal Crott - https://www.drupal.org/u/hydra
* Dominik Wille - https://www.drupal.org/u/harlor


SUPPORTING ORGANIZATIONS
-----------

* [erdfisch](https://www.drupal.org/erdfisch)
* [WERTGARANTIE Bike GmbH](https://www.drupal.org/wertgarantie-bike-gmbh)
