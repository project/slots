<?php

declare(strict_types=1);

namespace Drupal\Tests\slots\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Component\Serialization\Json;
use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;

/**
 * Test the slots module.
 *
 * @group slots
 */
final class SlotsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'slots',
    'block',
    'block_content',
    'system',
    'user',
    'conditions',
    'conditions_field',
    'block_plugin_view_builder',
    'field',
    'json_field',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('block_content');
    $this->installConfig(['field']);
  }

  /**
   * Test content is determined by SlotService::buildSlotContents() correctly.
   */
  public function testBuildSlotContents(): void {
    // Create a block content type.
    BlockContentType::create([
      'id' => 'spiffy',
      'label' => 'Mucho spiffy',
      'description' => "Provides a block type that increases your site's spiffiness by up to 11%",
    ])->save();

    $fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->create([
      'field_name' => 'field_slots',
      'entity_type' => 'block_content',
      'type' => 'slots',
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'label' => 'Slots',
      'bundle' => 'spiffy',
      'description' => 'Description for slots',
      'required' => FALSE,
    ]);
    $field->save();

    // And a block content entity.
    $slot_content = BlockContent::create([
      'info' => 'Spiffy prototype',
      'type' => 'spiffy',
    ]);
    $slot_content->set('field_slots', [
      'condition_logic' => 'and',
      'group_condition_logic' => 'or',
      'value' => '{"9546f5e5-010a-4a5b-9a72-52fd020373a8":{"id":"slot","uuid":"9546f5e5-010a-4a5b-9a72-52fd020373a8","slot_id":"not_my_slot","negate":false,"context_mapping":{"slot_id":"@slots.context_provider:slot"}}}',
      'slots_status' => 1,
      'status' => 1,
    ]);
    $slot_content->save();

    // And a block content entity.
    $slot_content = BlockContent::create([
      'info' => 'Spiffy prototype',
      'type' => 'spiffy',
    ]);
    $slot_content->set('field_slots', [
      'condition_logic' => 'and',
      'group_condition_logic' => 'or',
      'value' => '{"9546f5e5-010a-4a5b-9a72-52fd020373a9":{"id":"slot","uuid":"9546f5e5-010a-4a5b-9a72-52fd020373a9","slot_id":"my_slot","negate":false,"context_mapping":{"slot_id":"@slots.context_provider:slot"}}}',
      'slots_status' => 1,
      'status' => 1,
    ]);
    $slot_content->save();

    /** @var \Drupal\slots\SlotsServiceInterface $slots_service */
    $slots_service = \Drupal::service('slots.service');
    $build = $slots_service->buildSlotContents('my_slot');
    $lazy['entity_id'] = NULL;
    $i = 0;
    foreach ($build as $item) {
      if (!is_array($item) || !array_key_exists('slot_content_block', $item)) {
        continue;
      }

      $i++;
      $lazy = Json::decode($item['slot_content_block']['#lazy_builder'][1][1]);
    }

    self::assertEquals(1, $build['#length'], 'Wrong number of slot content');
    self::assertEquals($slot_content->id(), $lazy['entity_id'], 'Wrong content inside slot');
  }

}
