<?php

namespace Drupal\slots_twig\TwigExtension;

use Drupal\slots\SlotsServiceInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Slots twig extension.
 */
class SlotTwigExtension extends AbstractExtension implements SlotTwigExtensionInterface {

  /**
   * Builds a SlotTwigExtension object.
   *
   * @param \Drupal\slots\SlotsServiceInterface $slotService
   *   The slots service.
   */
  public function __construct(
    protected readonly SlotsServiceInterface $slotService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('slot',
        [$this->slotService, 'buildSlot'],
        ['is_safe' => ['html']]
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildSlot(string $slot_id, int $cardinality = 0): array {
    return $this->slotService->buildSlot($slot_id, $cardinality);
  }

}
