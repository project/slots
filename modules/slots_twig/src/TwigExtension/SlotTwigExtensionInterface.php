<?php

namespace Drupal\slots_twig\TwigExtension;

/**
 * Slots twig extension interface.
 */
interface SlotTwigExtensionInterface {

  /**
   * Builds the slots render array.
   *
   * @param string $slot_id
   *   The slot id to build the content for.
   * @param int $cardinality
   *   The cardinality of the slot.
   *
   * @return array
   *   A render array.
   */
  public function buildSlot(string $slot_id, int $cardinality = 0): array;

}
