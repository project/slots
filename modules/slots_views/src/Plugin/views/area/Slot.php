<?php

namespace Drupal\slots_views\Plugin\views\area;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\slots\SlotsServiceInterface;
use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area slot handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("slot")
 */
class Slot extends TokenizeAreaPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Creates a Slot instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\slots\SlotsServiceInterface $slotService
   *   The slots service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly SlotsServiceInterface $slotService,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('slots.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['slot_id'] = ['default' => Html::getId($this->view->id())];
    $options['cardinality'] = ['default' => 0];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['slot_id'] = [
      '#title' => $this->t('Slot identifier'),
      '#description' => $this->t('The slot identifier for which block type the content should be loaded.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['slot_id'],
      '#rows' => 6,
    ];
    $form['cardinality'] = [
      '#title' => $this->t('Slot cardinality'),
      '#description' => $this->t('The slot cardinality. Set to 0 to show unlimited slot items.'),
      '#type' => 'number',
      '#default_value' => $this->options['cardinality'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      $slot_id = $this->tokenizeValue($this->options['slot_id']);
      $slot_id = mb_strtolower(Html::cleanCssIdentifier($slot_id));
      return $this->slotService->buildSlot($slot_id, $this->options['cardinality']);
    }

    return [];
  }

}
