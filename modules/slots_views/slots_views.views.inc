<?php

/**
 * @file
 * Provide views data that isn't tied to any other module.
 */

/**
 * Implements hook_views_data().
 */
function slots_views_views_data() {
  $data['views']['area_slot'] = [
    'title' => t('Slot'),
    'help' => t('Provides a slot which can be used to push content into.'),
    'area' => [
      'id' => 'slot',
    ],
  ];

  return $data;
}
