<?php

declare(strict_types=1);

namespace Drupal\slots_inline_block\Access;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Allow access for inline blocks used by slots_inline_block.
 */
class SlotsInlineBlocksContentAccessAllowed implements AccessibleInterface {

  /**
   * {@inheritdoc}
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $condition = in_array($operation, ['view', 'update', 'delete']);
    return $return_as_object ? AccessResult::allowedIf($condition) : $condition;
  }

}
