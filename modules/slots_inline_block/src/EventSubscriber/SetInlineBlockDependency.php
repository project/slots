<?php

declare(strict_types=1);

namespace Drupal\slots_inline_block\EventSubscriber;

use Drupal\block_content\BlockContentEvents;
use Drupal\block_content\Event\BlockContentGetDependencyEvent;
use Drupal\slots\SlotsServiceInterface;
use Drupal\slots_inline_block\Access\SlotsInlineBlocksContentAccessAllowed;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * An event subscriber that returns an access dependency for inline blocks.
 */
class SetInlineBlockDependency implements EventSubscriberInterface {

  /**
   * Creates the SetInlineBlockDependency service.
   *
   * @param \Drupal\slots\SlotsServiceInterface $slotsService
   *   The slots service.
   */
  public function __construct(
    protected readonly SlotsServiceInterface $slotsService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockContentEvents::BLOCK_CONTENT_GET_DEPENDENCY => 'onGetDependency',
    ];
  }

  /**
   * Handles the BlockContentEvents::INLINE_BLOCK_GET_DEPENDENCY event.
   *
   * @param \Drupal\block_content\Event\BlockContentGetDependencyEvent $event
   *   The event.
   */
  public function onGetDependency(BlockContentGetDependencyEvent $event) {
    $block_content = $event->getBlockContentEntity();
    // Allow access to the inline block, if using a slot condition.
    if ($this->slotsService->entityHasSlotCondition($block_content)) {
      $event->setAccessDependency(new SlotsInlineBlocksContentAccessAllowed());
    }
  }

}
