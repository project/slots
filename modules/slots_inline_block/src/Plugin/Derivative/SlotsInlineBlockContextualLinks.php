<?php

declare(strict_types=1);

namespace Drupal\slots_inline_block\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\slots\SlotsServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for slots_inline_block related contextual links.
 */
class SlotsInlineBlockContextualLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Constructs a SlotsInlineBlockContextualLinks object.
   *
   * @param \Drupal\slots\SlotsServiceInterface $slotsService
   *   The slots service.
   */
  public function __construct(
    protected readonly SlotsServiceInterface $slotsService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('slots.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    foreach ($this->slotsService->getSlotEntityTypes() as $block_content_type_id => $block_content_type) {
      $this->derivatives["$block_content_type_id"] = array_merge($base_plugin_definition, [
        'route_name' => "slots_inline_block.add_block",
        'title' => $this->t('Add "@block_type_label" inline block to slot', ['@block_type_label' => $block_content_type->label()]),
        'group' => "slots_{$block_content_type_id}",
        'options' => [
          'attributes' => [
            'class' => ['use-ajax'],
            // @todo Consider adding support for alternative design patterns.
            'data-dialog-type' => 'dialog',
            'data-dialog-renderer' => 'off_canvas',
            'data-settings-tray-edit' => TRUE,
          ],
        ],
      ]);
    }

    return $this->derivatives;
  }

}
