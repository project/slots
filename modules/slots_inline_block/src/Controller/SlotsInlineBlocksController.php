<?php

declare(strict_types=1);

namespace Drupal\slots_inline_block\Controller;

use Drupal\block_content\BlockContentTypeInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\conditions_field\ConditionsFieldServiceInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for the slots inline block create route.
 */
class SlotsInlineBlocksController extends ControllerBase {

  /**
   * The block content storage.
   */
  protected EntityStorageInterface $blockContentStorage;

  /**
   * Build the SlotsInlineBlocksController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   UUID Service.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $conditionManager
   *   The plugin.manager.condition service.
   * @param \Drupal\conditions_field\ConditionsFieldServiceInterface $conditionsFieldService
   *   The conditions field service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFormBuilderInterface $entity_form_builder,
    protected readonly UuidInterface $uuid,
    protected readonly PluginManagerInterface $conditionManager,
    protected readonly ConditionsFieldServiceInterface $conditionsFieldService,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFormBuilder = $entity_form_builder;
    $this->blockContentStorage = $this->entityTypeManager->getStorage('block_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
      $container->get('uuid'),
      $container->get('plugin.manager.condition'),
      $container->get('slots.conditions_field.service')
    );
  }

  /**
   * Controller callback to build an add block form.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $block_content_type
   *   The block content type the block should be created for.
   * @param string $slot_id
   *   The slot_id to set as a slot condition.
   *
   * @return array
   *   The form.
   */
  public function addInlineBlockContent(BlockContentTypeInterface $block_content_type, string $slot_id): array {
    /** @var \Drupal\block_content\BlockContentInterface */
    $block_content = $this->blockContentStorage->create([
      'type' => $block_content_type->id(),
    ]);
    // Create inline created blocks as inline blocks like layout builder does.
    $block_content->setNonReusable();
    // Pre-populate the editorial_block condition on the conditions field.
    foreach ($this->conditionsFieldService->getConditionsFieldDefinitions($block_content->getEntityTypeId()) as $field_name => $field_definition) {
      if ($block_content->hasField($field_name)) {
        $block_content->set($field_name, [
          'value' => JSON::encode($this->createDefaultSlotConditionPlugin($slot_id)),
          'condition_logic' => 'and',
          'group_condition_logic' => 'or',
        ]);
        break;
      }
    }

    $form = $this->entityFormBuilder->getForm($block_content);
    // Hide configure option since we use the block as an inline block.
    unset($form['actions']['configure_block']);
    return $form;
  }

  /**
   * Creates the default slot condition configuration.
   *
   * @param string $slot_id
   *   The slot_id to set as a default value.
   *
   * @return array
   *   The prebuild condition config.
   */
  protected function createDefaultSlotConditionPlugin(string $slot_id): array {
    $uuid = $this->uuid->generate();
    $plugin = $this->conditionManager->createInstance('slot', ['slot_id' => $slot_id]);

    return [$uuid => $plugin->getConfiguration()];
  }

}
