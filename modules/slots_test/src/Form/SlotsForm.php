<?php

declare(strict_types=1);

namespace Drupal\slots_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\devel\DevelDumperManagerInterface;

/**
 * Provides a slots element test form.
 */
final class SlotsForm extends FormBase {

  /**
   * Constructs a new SlotsFormTest object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\devel\DevelDumperManagerInterface|null $develDumper
   *   The devel dumper manager service, if available.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    protected ?DevelDumperManagerInterface $develDumper = NULL,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('module_handler'),
      $container->has('devel.dumper') ? $container->get('devel.dumper') : NULL
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'slots_form_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['slots'] = [
      '#type' => 'slots',
      '#title' => $this->t('Slots'),
      '#plugin_manager' => 'plugin.manager.condition',
      '#default_value' => [],
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->cleanValues();

    if ($this->moduleHandler->moduleExists('devel') && $this->develDumper) {
      $this->develDumper->message($form_state->getValue('slots'));
    }
    else {
      $this->messenger->addStatus(print_r($form_state->getValue('slots'), TRUE));
    }
  }

}
