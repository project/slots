<?php

namespace Drupal\slots_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Drupal\slots\SlotIdMachineNameTrait;

/**
 * Provides a slot behavior plugin.
 *
 * @ParagraphsBehavior(
 *   id = "slot",
 *   label = @Translation("Slot"),
 *   description = @Translation("Provides the option add a slot to the paragraph with a configurable slot id."),
 * )
 */
class SlotBehavior extends ParagraphsBehaviorBase {

  use SlotIdMachineNameTrait;

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['has_custom_slot_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Provide custom slot id'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'has_custom_slot_id', 0),
    ];

    // Dynamically build path for states.
    $checkbox_parents = array_merge($form['#parents'], ['has_custom_slot_id']);
    $checkbox_path = array_shift($checkbox_parents);
    $checkbox_path .= '[' . implode('][', $checkbox_parents) . ']';

    $form['slot_id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this, 'slotIdExistsCallback'],
        'replace_pattern' => $this->getReplacePattern(),
        'error' => $this->t('The slot ID name must contain only lowercase letters, numbers, underscores and hyphens.'),
        'source' => ['#id' => FALSE],
      ],
      '#title' => $this->t('Custom slot id'),
      '#maxlength' => 255,
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'slot_id', ''),
      '#description' => $this->t('The custom slot id.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="' . $checkbox_path . '"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="' . $checkbox_path . '"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $value = $this->t('Slot id gets generated after the entity is saved.');

    if (!$paragraph->isNew()) {
      $value = $this->getSlotId($paragraph);
    }

    return [
      [
        'label' => $this->t('Slot id'),
        'value' => $value,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $build[] = [
      '#type' => 'inline_template',
      '#template' => '{{ slot(slot_id) }}',
      '#context' => [
        'slot_id' => $this->getSlotId($paragraph),
      ],
    ];
  }

  /**
   * Gets the slot id for the paragraph.
   *
   * Only if the ParagraphBehavior is activated,
   * either the custom id (set by the user) or an automatically
   * generated string is returned.
   *
   * @return string
   *   The slot id.
   */
  protected function getSlotId(ParagraphInterface $paragraph): string {
    if ((bool) $paragraph->getBehaviorSetting('slot', 'has_custom_slot_id')) {
      return $paragraph->getBehaviorSetting('slot', 'slot_id');
    }

    $slot_id = sprintf(
      'slot-%s-%s-%s',
      $paragraph->getParentEntity()->getEntityTypeId(),
      str_replace('_', '-', $paragraph->getParentEntity()->bundle()),
      $this->getParagraphPosition($paragraph)
    );

    return $slot_id;
  }

  /**
   * Determines the position of a paragraph in a paragraph field per bundle.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraphs_entity
   *   The bundle of the paragraph.
   *
   * @return string
   *   The position of the paragraph.
   */
  protected function getParagraphPosition(ParagraphInterface $paragraphs_entity): string {
    $field_name = $paragraphs_entity->get('parent_field_name')->value;
    $filtered_paragraphs = [];
    $count = 0;

    foreach ($paragraphs_entity->getParentEntity()->{$field_name}->referencedEntities() as $paragraph) {
      if ($paragraph->bundle() === $paragraphs_entity->bundle()) {
        $count++;
        $filtered_paragraphs[$paragraph->id()] = $count;
      }
    }

    return (string) $filtered_paragraphs[$paragraphs_entity->id()];
  }

}
