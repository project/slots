<?php

declare(strict_types=1);

namespace Drupal\slots\ContextProvider;

use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The slots context provider.
 */
class SlotsContext implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    // Don't provide a runtime context since we cannot determine one. To make
    // usage of this, the context needs to be created and injected into the
    // requiring object.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    return [
      'slot' => new Context(new ContextDefinition('slot', $this->t('The slot')), NULL),
    ];
  }

}
