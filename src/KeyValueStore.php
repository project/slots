<?php

declare(strict_types=1);

namespace Drupal\slots;

use Drupal\Core\KeyValueStore\DatabaseStorage;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a key-value store for slot IDs.
 */
class KeyValueStore implements KeyValueStoreInterface {

  /**
   * The key used for storing slot IDs.
   */
  const KEY = 'slot_ids';

  /**
   * Constructs a new KeyValueStore object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueStore
   *   The key-value store factory.
   */
  public function __construct(
    protected readonly KeyValueFactoryInterface $keyValueStore,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setValue(string $value): void {
    if (!$this->getStore()->has($value)) {
      $this->getStore()->set($value, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAll(): array {
    return $this->getStore()->getAll();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll(): void {
    $this->getStore()->deleteAll();
  }

  /**
   * Gets the database storage for slot IDs.
   *
   * @return \Drupal\Core\KeyValueStore\DatabaseStorage
   *   The database storage for slot IDs.
   */
  private function getStore(): DatabaseStorage {
    return $this->keyValueStore->get(self::KEY);
  }

}
