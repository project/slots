<?php

declare(strict_types=1);

namespace Drupal\slots\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Alters controller builds.
 */
class ControllerAlterSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Priority > 0 so that it runs before the controller output
    // is rendered by \Drupal\Core\EventSubscriber\MainContentViewSubscriber.
    $events[KernelEvents::VIEW][] = ['onView', 50];
    return $events;
  }

  /**
   * Alters the build of a controller view.
   *
   * @param \Symfony\Component\HttpKernel\Event\ViewEvent $event
   *   The controller view event.
   */
  public function onView(ViewEvent $event) {
    $route = $event->getRequest()->attributes->get('_route');

    // Manipulate the build of the layout_builder.choose_block route.
    if ($route == 'layout_builder.choose_block') {
      $build = $event->getControllerResult();

      // Add a custom 'Create slot' create link.
      if (isset($build['add_block'])) {
        // Add a 'Create slot' to the list.
        $build['add_slots_block'] = $build['add_block'];
        $build['add_slots_block']['#url'] = $build['block_categories'][$this->t('Slots')->__toString()]['links']['#links'][0]['url'];
        $build['add_slots_block']['#title'] = $this->t('Create slot');
        $build['add_slots_block']['#weight'] = -2;
      }

      // Remove the slots_block block from the list since its more
      // prominent now through the create link.
      if (isset($build['block_categories'][$this->t('Slots')->__toString()])) {
        unset($build['block_categories'][$this->t('Slots')->__toString()]);
      }

      $event->setControllerResult($build);
    }
  }

}
