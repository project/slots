<?php

namespace Drupal\slots;

/**
 * Provides slot_id machine_name related methods.
 */
trait SlotIdMachineNameTrait {

  /**
   * The machine_name exists callback.
   *
   * @return bool
   *   We always return FALSE because the slot_id doesn't have to be unique.
   */
  public function slotIdExistsCallback(): bool {
    return FALSE;
  }

  /**
   * The replacement pattern for a machine_name field.
   *
   * @return string
   *   The regex replacement pattern.
   */
  public function getReplacePattern(): string {
    return '[^a-z0-9_.-]+';
  }

}
