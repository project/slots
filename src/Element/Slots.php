<?php

declare(strict_types=1);

namespace Drupal\slots\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Attribute\FormElement;
use Drupal\Core\Render\Element\FormElementBase;

/**
 * Provides a slots element.
 *
 * Properties:
 * - #default_value: The conditions_groups values.
 * - #slots_status: The slots status disables the whole conditions_groups
 *   configuration part of the form.
 *
 * Usage example:
 * @code
 * $form['my_conditions_groups_element'] = [
 *   '#type' => 'slots',
 *   '#title' => $this->t('Slots'),
 *   '#slots_status' => TRUE,
 * ];
 * @endcode
 */
#[FormElement('slots')]
class Slots extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processPluginsElement'],
        [$class, 'buildPluginsForm'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes the plugins form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processPluginsElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    if (!isset($element['#slots_status'])) {
      $element['#slots_status'] = 0;
    }
    if ((!isset($form_state->getUserInput()['_drupal_ajax']) && !$form_state->getTriggeringElement())
      || ($form_state->getTriggeringElement() && $form_state->getTriggeringElement()['#ajax']['callback'][0] != static::class)) {
      $element['#slots_status'] = $element['#slots_status'];
      $element['#conditions_groups'] = $element['#default_value'];
    }
    // If we are on an ajax request coming from this object, take the data from
    // form state instead.
    else {
      $form_state->cleanValues();
      $element['#slots_status'] = $form_state->getValue($element['#parents'])['slots_status'][1] ?? 0;
      $element['#conditions_groups'] = $form_state->getValue($element['#parents'])['conditions_groups'] ?? [];

      // In case we have a hidden conditions_field, and it gets back into view,
      // restore the data from tmp store.
      if (!$element['#conditions_groups'] && $element['#slots_status'] == 1 && isset($form_state->getValue($element['#parents'])['conditions_groups_tmp'])) {
        $element['#conditions_groups'] = Json::decode($form_state->getValue($element['#parents'])['conditions_groups_tmp']);
        $input = &$form_state->getUserInput();
        NestedArray::setValue($input, array_merge($element['#parents'], ['conditions_groups']), $element['#conditions_groups']);
      }

      // If there is no tmp data, we try to get data from #default_value.
      if (!$element['#conditions_groups'] && $element['#slots_status'] == 1 && !empty($element['#default_value'])) {
        $element['#conditions_groups'] = $element['#default_value'];
      }
    }

    $wrapper_id = Html::getId('slots-form-element-' . $element['#name'] . '-' . implode('_', $element['#parents']) . '-wrapper');
    return array_merge($element, [
      '#tree' => TRUE,
      '#wrapper_id' => $wrapper_id,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ]);
  }

  /**
   * Builds the plugins form for the processed element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function buildPluginsForm(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $access = FALSE;
    if ((!empty($form_state->getUserInput()) && NestedArray::getValue($form_state->getUserInput(), array_merge(
          $element['#parents'],
          ['slots_status', 1],
        )) == 1)
      || $element['#slots_status']
    ) {
      $access = TRUE;
    }

    // If the conditions_groups element is not getting rendered, but we have
    // already submitted data, lets store it so it does not get lost.
    if (!$access && $form_state->getValue(array_merge($element['#parents'], ['conditions_groups']))) {
      $form_state->cleanValues();
      $element['conditions_groups_tmp'] = [
        '#type' => 'hidden',
        '#value' => Json::encode($element['#conditions_groups']),
      ];
    }

    $element['slots_status'] = [
      '#type' => 'checkboxes',
      '#default_value' => $element['#slots_status'] ? [1] : [],
      '#options' => [
        1 => t('Display this content in slot(s).'),
      ],
      '#depth' => -3,
      '#ajax' => [
        'callback' => [static::class, 'addPluginAjax'],
        'wrapper' => $element['#wrapper_id'],
        'effect' => 'fade',
        'progress' => ['type' => 'fullscreen'],
      ],
      '#limit_validation_errors' => [$element['#parents']],
    ];

    if ($access) {
      $element['conditions_groups'] = [
        '#type' => 'conditions_groups',
        '#default_value' => $element['#conditions_groups'] ?? [],
        '#parent_default_value' => $element['#default_value'] ?? [],
        '#condition_logic_locked' => TRUE,
        '#group_condition_logic_locked' => TRUE,
        '#conditions_title' => t('Additional conditions'),
        '#conditions_group_title' => t('Slots configuration group'),
      ];
      // Add custom processing to the element. Make sure the process method
      // comes before the build method since it relies on it's data.
      $info = \Drupal::service('plugin.manager.element_info')->createInstance('conditions_groups')->getInfo();
      $element['conditions_groups']['#process'] = array_merge(
        $info['#process'],
        [[static::class, 'processSlotsWidgetBuildConditionGroups']]
      );
      array_splice($element['conditions_groups']['#process'], 1, 0, [
        [static::class, 'slotsWidgetProcessConditionGroups'],
      ]);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input) {
      // When submitting the form and the slot status is disabled, set the
      // conditions groups data from tmp so it does not get lost.
      if (
        (
          isset($input['slots_status'])
          && $input['slots_status'][1] == 0
          && !isset($input['conditions_groups'])
          && isset($input['conditions_groups_tmp'])
        )
        || (
          !isset($input['slots_status'])
          && !isset($input['conditions_groups'])
          && isset($input['conditions_groups_tmp'])
        )
      ) {
        $input['conditions_groups'] = Json::decode($input['conditions_groups_tmp']);
        $input['slots_status'][1] = 0;
        unset($input['conditions_groups_tmp']);
      }
    }

    return $input ?: [];
  }

  /**
   * AJAX callback for adding a plugin.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The updated form fragment.
   */
  public static function addPluginAjax(array &$form, FormStateInterface $form_state): array {
    return NestedArray::getValue($form, array_slice($form_state->getTriggeringElement()['#array_parents'], 0, -2));
  }

  /**
   * Custom slot processing for a conditions groups element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function slotsWidgetProcessConditionGroups(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    // When using the slots_status checkbox, our values will get lost. So lets
    // make sure the values get properly re-added.
    foreach ($element["#conditions_groups"] as $conditions_group) {
      if ($conditions_group['status'] === NULL) {
        $element['#conditions_groups'] = $element["#parent_default_value"];
        break;
      }
    }

    return $element;
  }

  /**
   * Custom slot build processing for a conditions groups element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processSlotsWidgetBuildConditionGroups(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    // Add a default slot condition plugin to every conditions_group for better
    // UX, since it is mandatory anyway.
    foreach ($element["#conditions_groups"] as $uuid => $conditions_group) {
      if (empty($element[$uuid]['conditions']['#default_value'])) {
        $element[$uuid]['conditions']['#default_value'] = [
          \Drupal::service('uuid')->generate() => [
            'id' => 'slot',
            'configuration' => [
              'context_mapping' => [
                'slot_id' => '@slots.context_provider:slot',
              ],
            ],
          ],
        ];
        $element['#conditions_groups'][$uuid]['conditions'] = $element[$uuid]['conditions']['#default_value'];
        // Inform the form_state about our new input values.
        $input = &$form_state->getUserInput();
        NestedArray::setValue($input, $element['#parents'], $element['#conditions_groups']);
      }

      // Allow processing the condition.
      $info = \Drupal::service('plugin.manager.element_info')->createInstance('conditions')->getInfo();
      $element[$uuid]['conditions']['#process'] = array_merge(
        $info['#process'],
        [[static::class, 'slotsWidgetProcessConditions']]
      );
    }

    return $element;
  }

  /**
   * Custom slot processing for a conditions' element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function slotsWidgetProcessConditions(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    // Make the slot condition an item instead of putting it into the vertical
    // tabs for UX. It should always be visible since it's the major condition
    // to use this feature.
    foreach ($element['#plugins'] as $uuid => $plugins) {
      if ($plugins->getPluginId() == 'slot') {
        unset($element[$uuid]["#title"]);
        unset($element[$uuid]["#group"]);
        $element[$uuid]["#type"] = 'item';
        $element[$uuid]["#weight"] = -2;
        $element[$uuid]["actions"]['#access'] = FALSE;
        $element[$uuid]["#after_build"][] = [
          static::class,
          'slotsWidgetConditionsAfterBuild',
        ];
      }
    }

    return $element;
  }

  /**
   * Disables the negate option on slot conditions.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The element.
   */
  public static function slotsWidgetConditionsAfterBuild(array $element, FormStateInterface $form_state): array {
    // Disable the negate function on slot conditions per default since it
    // pollutes the UI and leads to confusion.
    $element['configuration']['negate']['#access'] = FALSE;

    return $element;
  }

}
