<?php

declare(strict_types=1);

namespace Drupal\slots;

use Drupal\block_plugin_view_builder\BlockPluginViewBuilderInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\conditions\ConditionsServiceInterface;
use Drupal\conditions_field\ConditionsFieldServiceInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;

/**
 * The slots service.
 */
class SlotsService implements SlotsServiceInterface {

  use ConditionAccessResolverTrait;
  use StringTranslationTrait;

  /**
   * The slot entity types.
   */
  protected array $slotEntityTypes = [];

  /**
   * Constructs a SlotsService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $conditionManager
   *   The plugin.manager.condition service.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $contextRepository
   *   The context repository service.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $contextHandler
   *   The plugin context handler.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\conditions_field\ConditionsFieldServiceInterface $conditionsFieldService
   *   The conditions field service.
   * @param \Drupal\block_plugin_view_builder\BlockPluginViewBuilderInterface $blockPluginViewBuilder
   *   The block plugin view builder service.
   * @param \Drupal\conditions\ConditionsServiceInterface $conditionsService
   *   The conditions service.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly AccountInterface $currentUser,
    protected readonly PluginManagerInterface $conditionManager,
    protected readonly ContextRepositoryInterface $contextRepository,
    protected readonly ContextHandlerInterface $contextHandler,
    protected readonly Connection $database,
    protected readonly ConditionsFieldServiceInterface $conditionsFieldService,
    protected readonly BlockPluginViewBuilderInterface $blockPluginViewBuilder,
    protected readonly ConditionsServiceInterface $conditionsService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getSlotContext(string $slot_id, string $context_id = '@slots.context_provider:slot'): array {
    $context = new Context(new ContextDefinition('slot', $this->t('The slot.')), $slot_id);
    return [
      $context_id => $context,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildSlot(string $slot_id, int $cardinality = 0): array {
    $build = [];
    $build['#cache']['tags'][] = 'slot_id:' . $slot_id;

    $slot = $this->blockPluginViewBuilder->view('slot_block', [
      'slot_id' => $slot_id,
      'cardinality' => $cardinality,
    ]);
    $build[] = $slot;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSlotContents(string $slot_id): array {
    $build = ['#length' => 0];

    // Find out which potential cache metadata needs to be taken care of.
    $cacheable_metadata = new CacheableMetadata();
    if ($condition_collections = $this->getMatchingConditions($slot_id, 'block_content')) {
      foreach ($condition_collections as $conditions) {
        foreach ($conditions as $condition) {
          $cacheable_metadata->addCacheableDependency($condition);
        }
      }
    }
    $cacheable_metadata->applyTo($build);
    // Add slot_id cache tag, so we can clear the cache for every slot
    // precisely.
    $build['#cache']['tags'][] = 'slot_id:' . $slot_id;

    // Add the blocks we found to the render array.
    if ($matching_items = $this->getMatchingItemsBySlotId($slot_id, 'block_content')) {
      foreach ($matching_items as $entity_id => $metadata) {
        $block_content_display = $this->blockPluginViewBuilder->view(
          'slot_content_block',
          [
            'entity_type_id' => 'block_content',
            'entity_id' => $entity_id,
          ]
        );
        $build[] = $block_content_display;
        $build['#length']++;
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getSlotEntityTypes(): array {
    if (empty($this->slotEntityTypes)) {
      $conditions_fields = $this->conditionsFieldService->getConditionsFieldDefinitions('block_content');
      foreach ($conditions_fields as $field_map) {
        foreach ($field_map['bundles'] as $bundle) {
          $this->slotEntityTypes[$bundle] = $this->entityTypeManager->getStorage('block_content_type')->load($bundle);
        }
      }
    }

    return $this->slotEntityTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function getMatchingItemsBySlotId(string $slot_id, string $entity_type_id): ?array {
    $matching_items = $this->conditionsFieldService->getMatchingItems(
      $entity_type_id,
      [
        'id' => 'slot',
        'slot_id' => $slot_id,
      ],
      $this->getSlotContext($slot_id),
      [
        'status' => '1',
        'slots_status' => '1',
      ],
    );
    return !empty($matching_items) ? $matching_items : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMatchingConditions(string $slot_id, string $entity_type_id): ?array {
    // @todo Unify with ConditionsFieldService::getMatchingItems()
    // Load all potential items from database.
    $matching_items = $this->conditionsFieldService->getItemsDataByConditionConfiguration(
      $entity_type_id,
      [
        'id' => 'slot',
        'slot_id' => $slot_id,
      ],
      [
        'status' => '1',
        'slots_status' => '1',
      ],
    );

    // Build the conditions used in the slots field.
    $condition_collections = [];
    foreach ($matching_items as $condition_groups) {
      // We need to initialize the conditions in order to get their cacheable
      // metadata.
      $this->conditionsService->initializeConditions($condition_groups, []);
      foreach ($condition_groups as $condition_group) {
        $condition_collections[] = $condition_group['conditions'];
      }
    }

    return !empty($condition_collections) ? $condition_collections : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function entityHasSlotCondition(EntityInterface $entity): bool {
    $condition_fields = $this->conditionsFieldService->getConditionsFieldDefinitions($entity->getEntityTypeId());
    foreach ($condition_fields as $field_name => $field_definition) {
      // Make sure the bundle of our entity uses the field.
      if ($entity->hasField($field_name)) {
        foreach ($entity->get($field_name) as $field_condition) {
          // The conditions field stores the conditions in a json field.
          $conditions = !empty($field_condition->value) ? JSON::decode($field_condition->value) : [];
          foreach ($conditions as $condition) {
            // If a condition with the id "slot" exists, we return TRUE.
            if (isset($condition['id']) && $condition['id'] == 'slot') {
              return TRUE;
            }
          }
        }
      }
    }

    return FALSE;
  }

}
