<?php

declare(strict_types=1);

namespace Drupal\slots;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for the slots service.
 */
interface SlotsServiceInterface {

  /**
   * Gets the contexts we need for our condition to work.
   *
   * @param string $slot_id
   *   The slot id for the context.
   * @param string $context_id
   *   An optional context_id to get the context from.
   *
   * @return \Drupal\Core\Plugin\Context\ContextInterface[]
   *   An array of set contexts.
   */
  public function getSlotContext(string $slot_id, string $context_id = '@slots.context_provider:slot'): array;

  /**
   * Builds a slot.
   *
   * @param string $slot_id
   *   The slot id to build.
   * @param int $cardinality
   *   The cardinality for the slot.
   *
   * @return array
   *   A render array.
   */
  public function buildSlot(string $slot_id, int $cardinality = 0): array;

  /**
   * Builds the slots render array.
   *
   * @param string $slot_id
   *   The slot id to build the content for.
   *
   * @return array
   *   A render array.
   */
  public function buildSlotContents(string $slot_id): array;

  /**
   * Determines the entity types which support slots.
   *
   * We consider an entity type to support slots, when it uses the slots field.
   * Without this field, the entities can't make usage of the slot condition.
   *
   * @return array
   *   The entity types.
   */
  public function getSlotEntityTypes(): array;

  /**
   * Get the matching items for a given slot_id.
   *
   * @param string $slot_id
   *   The slot_id which has been used by the slot condition.
   * @param string $entity_type_id
   *   The entity type id to search for matching items.
   *
   * @return array|null
   *   An array of matching items as provided by
   *   ConditionsFieldServiceInterface::getMatchingItems.
   */
  public function getMatchingItemsBySlotId(string $slot_id, string $entity_type_id): ?array;

  /**
   * Get the matching conditions data for a given slot_id.
   *
   * @param string $slot_id
   *   The slot_id which has been used by the slot condition.
   * @param string $entity_type_id
   *   The entity type id to search for matching items.
   *
   * @return array|null
   *   An array of condition collections built from the data provided by
   *   ConditionsFieldServiceInterface::getItemsDataByConditionConfiguration.
   */
  public function getMatchingConditions(string $slot_id, string $entity_type_id): ?array;

  /**
   * Determines if the slot condition is configured for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity which uses the condition.
   *
   * @return bool
   *   TRUE if it uses the slot condition.
   */
  public function entityHasSlotCondition(EntityInterface $entity): bool;

}
