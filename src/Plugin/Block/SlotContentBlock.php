<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\Block;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Slot content block".
 *
 * @Block(
 *   id = "slot_content_block",
 *   admin_label = @Translation("Slot content block")
 * )
 */
class SlotContentBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Creates a SlotContentBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($entity = $this->getEntity()) {
      return $this->entityTypeManager
        ->getViewBuilder($this->configuration['entity_type_id'])
        ->view($entity);
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    if ($entity = $this->getEntity()) {
      return $entity->access('view', $account, $return_as_object);
    }

    return AccessResult::neutral();
  }

  /**
   * Get entity from configuration.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  protected function getEntity(): ?EntityInterface {
    if (
      array_key_exists('entity_type_id', $this->configuration) &&
      array_key_exists('entity_id', $this->configuration)
    ) {
      return $this->entityTypeManager
        ->getStorage($this->configuration['entity_type_id'])
        ->load($this->configuration['entity_id']);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // #3460195 invalidate cache on content change.
    $tags = parent::getCacheTags();
    return Cache::mergeTags($tags, $this->getEntity()->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    return Cache::mergeContexts($contexts, $this->getEntity()->getCacheContexts());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $max_age = parent::getCacheMaxAge();
    return Cache::mergeMaxAges($max_age, $this->getEntity()->getCacheMaxAge());
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array_merge(parent::defaultConfiguration(), [
      'view_mode' => 'full',
    ]);
  }

}
