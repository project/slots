<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\Block;

use Drupal\block_plugin_view_builder\BlockPluginViewBuilderInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\slots\KeyValueStoreInterface;
use Drupal\slots\SlotIdMachineNameTrait;
use Drupal\slots\SlotsServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a slot block.
 *
 * @Block(
 *   id = "slot_block",
 *   admin_label = @Translation("Slot block")
 * )
 */
class SlotBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use SlotIdMachineNameTrait;

  /**
   * Creates a SlotBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\slots\SlotsServiceInterface $slotsService
   *   The slots service.
   * @param \Drupal\block_plugin_view_builder\BlockPluginViewBuilderInterface $blockPluginViewBuilder
   *   The block plugin view builder service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Component\Uuid\UuidInterface $uuidService
   *   The UUID service.
   * @param \Drupal\slots\KeyValueStoreInterface $keyValueStore
   *   Interface for key-value store operations.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly SlotsServiceInterface $slotsService,
    protected readonly BlockPluginViewBuilderInterface $blockPluginViewBuilder,
    protected readonly AccountInterface $currentUser,
    protected readonly UuidInterface $uuidService,
    protected readonly KeyValueStoreInterface $keyValueStore,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('slots.service'),
      $container->get('block_plugin.view_builder'),
      $container->get('current_user'),
      $container->get('uuid'),
      $container->get('slots.key_value_store')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $this->keyValueStore->setValue($this->configuration['slot_id']);

    if (!$this->configuration['empty']) {
      $build = $this->slotsService->buildSlotContents($this->configuration['slot_id']);
      if ($this->configuration['cardinality'] > $build['#length'] || $this->configuration['cardinality'] == 0) {
        $build[] = $this->blockPluginViewBuilder->view('slot_block', [
          'slot_id' => $this->configuration['slot_id'],
          'empty' => TRUE,
        ]);
      }

      $build['#attributes']['class'][] = 'is-slot-wrapper';
    }
    else {
      if ($this->currentUser->hasPermission('view slot identifiers')) {
        $build[] = [
          '#type' => 'inline_template',
          '#template' => '<div class="slots--slot__identifier">{{ content }}</div>',
          '#context' => [
            'content' => $this->t('Slot with the id: @slot_id', ['@slot_id' => new FormattableMarkup('<pre>' . $this->configuration['slot_id'] . '</pre>', [])]),
          ],
        ];
        $this->configuration['empty'] = TRUE;
        $build['#attributes']['class'][] = 'is-empty-slot';
      }
    }

    if (!empty($build)) {
      $build['#attributes']['class'][] = 'slots--slot';
      $build['#attached']['library'][] = 'slots/drupal.slots.theme';
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Remove label display settings, since this block is only used as a
    // container.
    $form['label']['#access'] = FALSE;
    $form['label']['#value'] = '';
    $form['label_display']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form['slot_id'] = [
      '#title' => $this->t('Slot identifier'),
      '#description' => $this->t('The slot identifier for which block type the content should be loaded.'),
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this, 'slotIdExistsCallback'],
        'replace_pattern' => $this->getReplacePattern(),
        'error' => $this->t('The slot ID name must contain only lowercase letters, numbers, underscores and hyphens.'),
      ],
      '#default_value' => empty($this->configuration['slot_id']) ? $this->uuidService->generate() : $this->configuration['slot_id'],
      '#disabled' => !empty($this->configuration['slot_id']),
    ];
    $form['cardinality'] = [
      '#title' => $this->t('Slot cardinality'),
      '#description' => $this->t('The slot cardinality. Set to 0 to show unlimited slot items.'),
      '#type' => 'number',
      '#default_value' => $this->configuration['cardinality'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['slot_id'] = $form_state->getValue('slot_id');
    $this->configuration['cardinality'] = $form_state->getValue('cardinality');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array_merge(parent::defaultConfiguration(), [
      'slot_id' => '',
      'cardinality' => 0,
      'empty' => FALSE,
    ]);
  }

}
