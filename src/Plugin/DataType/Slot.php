<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * The "slot" data type.
 *
 * @DataType(
 *   id = "slot",
 *   label = @Translation("Slot")
 * )
 */
class Slot extends StringData {

}
