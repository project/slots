<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\Condition;

use Drupal\Core\Condition\Attribute\Condition;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\slots\KeyValueStoreInterface;
use Drupal\slots\SlotIdMachineNameTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Slot' condition.
 */
#[Condition(
  id: "slot",
  label: new TranslatableMarkup("Slot"),
  context_definitions: [
    "slot_id" => new ContextDefinition(
      data_type: "slot",
      label: new TranslatableMarkup("The slot id"),
    ),
  ]
)]
class SlotCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  use SlotIdMachineNameTrait;

  /**
   * Constructs a new SlotCondition object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\slots\KeyValueStoreInterface $key_value_store
   *   The key value store service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly KeyValueStoreInterface $key_value_store,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('slots.key_value_store')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array_merge(parent::defaultConfiguration(), [
      'slot_id' => '',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['slot_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Slot identifier'),
      '#options' => $this->key_value_store->getAll(),
      '#default_value' => $this->configuration['slot_id'],
      '#empty_option' => $this->t('- Select a block -'),
      '#required' => TRUE,
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['slot_id'] = $form_state->getValue('slot_id');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if (!empty($this->configuration['negate'])) {
      return $this->t('Do not apply for slots with the id: %id', ['%id' => $this->configuration['slot_id']]);
    }

    return $this->t('Apply for slots with the id: %id', ['%id' => $this->configuration['slot_id']]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $slot_id = $this->getContext('slot_id')->getContextValue();
    if (!empty($this->configuration['negate'])) {
      return $this->configuration['slot_id'] !== $slot_id;
    }

    return $this->configuration['slot_id'] === $slot_id;
  }

}
