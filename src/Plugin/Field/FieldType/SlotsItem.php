<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\Field\FieldType;

use Drupal\conditions_field\Plugin\Field\FieldType\ConditionsItem;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'slots' field type.
 */
#[FieldType(
  id: "slots",
  label: new TranslatableMarkup("Slots"),
  description: new TranslatableMarkup("Allows the content to be displayed inside slots."),
  default_widget: "slots",
  default_formatter: "slots_empty_formatter",
  cardinality: -1,
  constraints: ["valid_json" => []],
)]
class SlotsItem extends ConditionsItem implements SlotsItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['slots_status'] = [
      'description' => 'Whether the content shall be displayed in slots.',
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'default' => 1,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['slots_status'] = DataDefinition::create('integer')
      ->setLabel(t('Whether the content shall be displayed in slots'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getSlotsStatus(): bool {
    return $this->getValue()['slots_status'] == 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsOperator(): string {
    // Only 'and' makes sense in the context of slots.
    return 'and';
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsGroupOperator(): string {
    // Only 'or' makes sense in the context of slots.
    return 'or';
  }

}
