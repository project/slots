<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\Field\FieldType;

/**
 * Defines the slots item interface.
 */
interface SlotsItemInterface {

  /**
   * Check whether the content shall be displayed in slots.
   *
   * @return bool
   *   TRUE, if the content shall be displayed in slots.
   */
  public function getSlotsStatus(): bool;

}
