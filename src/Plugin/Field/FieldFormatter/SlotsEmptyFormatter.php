<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\Field\FieldFormatter;

use Drupal\conditions_field\Plugin\Field\FieldFormatter\ConditionsEmptyFormatter;

/**
 * Plugin implementation of the 'SlotsEmptyFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "slots_empty_formatter",
 *   module = "slots",
 *   label = @Translation("Empty formatter"),
 *   field_types = {
 *     "slots"
 *   }
 * )
 */
class SlotsEmptyFormatter extends ConditionsEmptyFormatter {

}
