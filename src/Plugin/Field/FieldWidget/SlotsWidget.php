<?php

declare(strict_types=1);

namespace Drupal\slots\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\conditions_field\Plugin\Field\FieldWidget\ConditionsGroupsWidget;
use Drupal\Core\Cache\Cache;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the 'slots' field widget.
 */
#[FieldWidget(
  id: 'slots',
  label: new TranslatableMarkup('Slots (EXPERIMENTAL)'),
  description: new TranslatableMarkup('A widget for configuring slots with conditions groups.'),
  field_types: ['slots'],
  multiple_values: TRUE,
)]
class SlotsWidget extends ConditionsGroupsWidget {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_name = $this->fieldDefinition->getName();
    // Since we can lose our values when triggering the status checkbox, let's
    // make sure our parent widget won't generate new ones with new uuid's. This
    // would lead to duplicate entries.
    $default_value = NestedArray::getValue(
      $form_state->getUserInput(),
      array_merge($element['#field_parents'], [$field_name, 'conditions_groups'])
    ) ?? [];
    foreach ($default_value as $conditions_group) {
      if ($conditions_group['status'] === NULL) {
        $default_value = NULL;
        break;
      }
    }
    // Reuse existing values if not broken.
    if ($default_value) {
      $element['#default_value'] = $default_value;
    }

    return array_merge($element, [
      '#type' => 'slots',
      '#slots_status' => !$items->isEmpty() ? $items->first()->getSlotsStatus() : 0,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values.
    $slots_status = NestedArray::getValue(
      $form_state->getValues(),
      array_merge($form['#parents'], [$field_name, 'slots_status', 1])
    );
    $conditions_groups = NestedArray::getValue(
      $form_state->getValues(),
      array_merge($form['#parents'], [$field_name, 'conditions_groups'])
    ) ?? [];

    // Let the parent widget do the work and append the slots_status.
    $this->generateItemsByConditionsGroups($items, $conditions_groups, $form, $form_state);
    foreach ($items as $item) {
      $item->set('slots_status', $slots_status);
    }

    // Handle caching if the form got submitted.
    if (!isset($form_state->getTriggeringElement()['#ajax'])) {
      $old_slot_ids = $this->extractSlotIds($items);
      $new_sot_ids = $this->extractSlotIds($items);

      // Invalidate all cache tags of the form slot_id:ID.
      $cache_tags = array_map(function ($s) {
        return 'slot_id:' . $s;

      }, array_merge($old_slot_ids, $new_sot_ids));
      $cache_tags[] = 'slot_list';
      Cache::invalidateTags(array_unique($cache_tags));
    }
  }

  /**
   * Extract slot ids from items.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field list items to extract the slot ids from.
   *
   * @return array
   *   The slot ids.
   */
  protected function extractSlotIds(FieldItemListInterface $items): array {
    $slot_ids = [];
    foreach ($items->getValue() as $condition_group) {
      if (empty($condition_group['value'])) {
        continue;
      }
      foreach (Json::decode($condition_group['value']) as $condition) {
        if (!isset($condition['id'])
          || (isset($condition['id']) && $condition['id'] != 'slot')
        ) {
          continue;
        }

        $slot_ids[] = $condition['slot_id'];
      }
    }

    return $slot_ids;
  }

}
