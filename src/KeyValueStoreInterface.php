<?php

declare(strict_types=1);

namespace Drupal\slots;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface for key-value store operations.
 */
interface KeyValueStoreInterface {

  /**
   * Creates a new instance of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   *
   * @return static
   *   A new instance of this class.
   */
  public static function create(ContainerInterface $container): self;

  /**
   * Sets a value in the key-value store.
   *
   * @param string $value
   *   The value to set.
   */
  public function setValue(string $value): void;

  /**
   * Gets all values from the key-value store.
   *
   * @return array
   *   An array of all values in the store.
   */
  public function getAll(): array;

  /**
   * Deletes all values from the key-value store.
   */
  public function deleteAll(): void;

}
